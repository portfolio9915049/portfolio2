<div align="center">

  <img src="assets/logo.png" alt="logo" width="200" height="auto" />
  <h1>Portfolio 2</h1>
  
  <p>
    This is a portfolio web template using Bootstrap, designed by Bootstrapmade. 
  </p>
  
  
<!-- Badges -->
<p>
  <a href="https://gitlab.com/portfolio9915049/portfolio1/graphs/contributors">
    <img src="https://img.shields.io/gitlab/contributors/portfolio9915049/portfolio1" alt="contributors" />
  </a>
  <a href="">
    <img src="https://img.shields.io/gitlab/last-commit/portfolio9915049/portfolio1" alt="last update" />
  </a>
  <a href="https://gitlab.com/portfolio9915049/portfolio1/network/members">
    <img src="https://img.shields.io/gitlab/forks/portfolio9915049/portfolio1" alt="forks" />
  </a>
  <a href="https://gitlab.com/portfolio9915049/portfolio1/stargazers">
    <img src="https://img.shields.io/gitlab/stars/portfolio9915049/portfolio1" alt="stars" />
  </a>
  <a href="https://gitlab.com/portfolio9915049/portfolio1/issues/">
    <img src="https://img.shields.io/gitlab/issues/portfolio9915049/portfolio1" alt="open issues" />
  </a>
  <a href="https://gitlab.com/portfolio9915049/portfolio1/blob/master/LICENSE">
    <img src="https://img.shields.io/gitlab/license/portfolio9915049/portfolio1.svg" alt="license" />
  </a>
</p>
   
<h4>
    <a href="https://gitlab.com/portfolio9915049/portfolio1/">View Demo</a>
  <span> · </span>
    <a href="https://gitlab.com/portfolio9915049/portfolio1">Documentation</a>
  <span> · </span>
    <a href="https://gitlab.com/portfolio9915049/portfolio1/issues/">Report Bug</a>
  <span> · </span>
    <a href="https://gitlab.com/portfolio9915049/portfolio1/issues/">Request Feature</a>
  </h4>
</div>

<br />

<!-- Table of Contents -->

# :notebook_with_decorative_cover: Table of Contents

-   [About the Project](#star2-about-the-project)
    -   [Screenshots](#camera-screenshots)
    -   [Tech Stack](#space_invader-tech-stack)
    -   [Features](#dart-features)
    -   [Code of Conduct](#scroll-code-of-conduct)
-   [FAQ](#grey_question-faq)
-   [License](#warning-license)
-   [Contact](#handshake-contact)
-   [Acknowledgements](#gem-acknowledgements)

<!-- About the Project -->

## :star2: About the Project

<!-- Screenshots -->

### :camera: Screenshots

<div align="center"> 
  <img src="assets/ss/pic1.png" alt="screenshot 1" />
  <img src="assets/ss/pic2.png" alt="screenshot 2" />
  <img src="assets/ss/pic3.png" alt="screenshot 3" />
  <img src="assets/ss/pic4.png" alt="screenshot 4" />
  <img src="assets/ss/pic5.png" alt="screenshot 5" />
  <img src="assets/ss/pic6.png" alt="screenshot 6" />
  <img src="assets/ss/pic7.png" alt="screenshot 7" />
  <img src="assets/ss/pic8.png" alt="screenshot 8" />
</div>

<!-- TechStack -->

### :space_invader: Tech Stack

<details>
  <summary>Client</summary>
  <ul>
    <li><a href="https://getbootstrap.com/">Bootstrap</a></li>
  </ul>
</details>

<details>
  <summary>Server</summary>
  <ul>
  </ul>
</details>

<details>
<summary>Database</summary>
  <ul>
  </ul>
</details>

<details>
<summary>DevOps</summary>
  <ul>
  </ul>
</details>

<!-- Features -->

### :dart: Features

-   Portfolio template

<!-- Code of Conduct -->

### :scroll: Code of Conduct

Please read the [Code of Conduct](https://github.com/Louis3797/awesome-readme-template/blob/master/CODE_OF_CONDUCT.md)

<!-- FAQ -->

## :grey_question: FAQ

-   Question 1

    -   Answer 1

-   Question 2

    -   Answer 2

<!-- License -->

## :warning: License

Distributed under the no License. See LICENSE.txt for more information.

<!-- Contact -->

## :handshake: Contact

Your Name - [@pektografi](https://twitter.com/pektografi) - irfanzulkarnain.dev@yahoo.com

Project Link: [https://gitlab.com/portfolio9915049/portfolio1](https://gitlab.com/portfolio9915049/portfolio1)

<!-- Acknowledgments -->

## :gem: Acknowledgements

Use this section to mention useful resources and libraries that you have used in your projects.

-   [Bootstrap](https://getbootstrap.com/)
-   [Shields.io](https://shields.io/)
-   [Awesome README](https://github.com/matiassingers/awesome-readme)
-   [Emoji Cheat Sheet](https://github.com/ikatyang/emoji-cheat-sheet/blob/master/README.md#travel--places)
-   [Readme Template](https://github.com/othneildrew/Best-README-Template)
